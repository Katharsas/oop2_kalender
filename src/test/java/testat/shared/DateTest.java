package testat.shared;

import static org.junit.Assert.*;

import org.junit.Test;

import testat.shared.model.Date;

/**
 * Testing some critical & normal date calculations.
 * 
 * @author Jan Mothes
 */
public class DateTest {

	@Test
	public void test() {
		
		Date date0 = Date.getDate(1,1,1999);
		assertEquals("01.01.1999, FRIDAY, Week: 53 (1998)", date0.toString());
		
		Date date1 = Date.getDate(2, 1, 2000);
		assertEquals("02.01.2000, SUNDAY, Week: 52 (1999)", date1.toString());
		
		Date date2 = Date.getDate(22, 8, 2008);
		assertEquals("22.08.2008, FRIDAY, Week: 34", date2.toString());
		
		Date date3 = Date.getDate(29, 12, 2009);
		assertEquals("29.12.2009, TUESDAY, Week: 53", date3.toString());
		
		//This is the static monday from Date class
		Date date4 = Date.getDate(13, 10, 2014);
		assertEquals("13.10.2014, MONDAY, Week: 42", date4.toString());
		
		Date date5 = Date.getDate(20, 2, 2015);
		assertEquals("20.02.2015, FRIDAY, Week: 8", date5.toString());
		
		Date date6 = Date.getDate(31, 12, 2018);
		assertEquals("31.12.2018, MONDAY, Week: 1 (2019)", date6.toString());
		
		//test argument checks
		boolean thrown;
		
		thrown = false;
		try {
			Date.getDate(32, 12, 2018);
		} catch (IllegalArgumentException e) {thrown = true;}
		assertTrue(thrown);
		
		thrown = false;
		try {
			Date.getDate(15, -1, 2015);
		} catch (IllegalArgumentException e) {thrown = true;}
		assertTrue(thrown);
	}

}
