package testat.shared;

import org.junit.Test;

import testat.shared.TablePrinter;

public class TablePrinterTest {

	@Test
	public void test() {
		TablePrinter table = new TablePrinter();
		int[] size = {10, 0, 15, 20};
		String[] row = {"daad", " dadda a da ", "32423423442", "234324"};
		String[] row1 = {"daadadasd", " dadda a  ", "3242342", "234adsadsda"};
		String[] row2 = {"daadasd", " dadda a da ", "32423423sd442", "23432433"};
		String[] row3 = {"daad", " dadda a da ", "32423423442", "234324", "xx"};
		table.addRow(row);
		table.addRow(row1);
		table.addRow(row2);
		table.addRow(row3);
		table.add("Name").add("Desc").finishRow();
		
		table.setMinWidth(size);
		
		System.out.println(table);
	}

}
