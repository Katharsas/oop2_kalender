package testat.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedOutputStream;
import java.net.Socket;
import java.util.List;

import javafx.application.Application;
import testat.client.gui.JavaFxGui;
import testat.shared.model.Appointment;
import testat.shared.net.ProtocolListener;
import testat.shared.net.ProtocolState;
import testat.shared.net.ProtocolState.ProtocolViolationException;
import testat.shared.net.SocketConnection;
import testat.shared.net.SocketSide;
import testat.shared.net.ProtocolWriter;

/**
 * http://code.makery.ch/library/javafx-8-tutorial/part1/
 * 
 * http://www.korecky.org/?p=678
 * 
 * @author Jan Mothes
 */
public class Client extends SocketSide {
	
	public static void main(String[] args) throws IOException {
		instance = new Client();
		new Thread(instance).start();
	}

	private static Client instance;
	public static Client getInstance() {return instance;}
	
	private ClientProtocolService protocolService;
	private final PipedOutputStream toJavaFxPipe = new PipedOutputStream();
	
	public ClientProtocolService getProtocolService() {
		synchronized (protocolService) {
			return protocolService;
		}
	}
	
	public PipedOutputStream getJavaFxPipe() {
		return toJavaFxPipe;
	}
	
	private JavaFxWriter toJavaFx;

	private Client() throws IOException {
		super(new Socket(SocketConnection.SERVER_IP, SocketConnection.PORT));
		
		Thread shell = new Thread(new ShellUserInterface(this));
		shell.start();
		Thread gui = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println(getSideName() + ": Started GUI thread");
				Application.launch(JavaFxGui.class);
				System.out.println(getSideName() + ": GUI thread ended");
			}
		});
		gui.start();
	}
	
	@Override
	protected ProtocolListener createProtocolInput(InputStream inputStream)
			throws IOException {
		return new ClientProtocolListener(this, inputStream);
	}
	
	@Override
	protected ProtocolWriter createProtocolOutput(OutputStream outputStream)
			throws IOException {
		protocolService = new ClientProtocolService(this, outputStream);
		return protocolService;
	}
	
	public JavaFxWriter getJavaFxService() throws IOException {
		if (toJavaFx == null) toJavaFx = new JavaFxWriter(this, toJavaFxPipe);
		return toJavaFx;
	}
	
	public boolean isServer() {
		return false;
	}

	
	/**
	 * Implements logic triggered by receiving server messages.
	 * 
	 * @author Jan Mothes
	 */
	public static class ClientProtocolListener extends ProtocolListener {
		
		private final Client client;
		private final ClientData data;
		private JavaFxWriter toJavaFx;
		
		public ClientProtocolListener(Client client, InputStream inputStream) throws IOException {
			super(client, inputStream);
			this.client = client;
			data = ClientData.getInstance();
			
		}
		
		/**
		 * TODO:
		 * Move this end of pipe to client so that other threads can access it too (like Service)
		 */
		@Override
		public void run() {
			try {
				synchronized (this) {
					wait(1000);//wait for GUI thread to connect pipe
				}
				toJavaFx = client.getJavaFxService();
			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
			super.run();
		}
		
		@Override
		protected void onMessageReceived(String message) throws ProtocolViolationException, IOException {
			switch (message) {
			
			// [CONNECTED]
			case ProtocolState.LOGIN_SUCCESSFUL:
				toJavaFx.onLoginSuccess();
				client.setState(ProtocolState.LOGGED_IN);
				break;
			case ProtocolState.LOGIN_FAILED:
				toJavaFx.onLoginFailed();
				break;
			case ProtocolState.CLOSE:
				toJavaFx.close();
				break;
				
			// [LOGGED_IN]
			case ProtocolState.PUSH_APPOS:
				onPush();
				break;
			
			default: throwIllegalProtocolMessageException(message);
			}
		}

		private void onPush() throws ProtocolViolationException {
			@SuppressWarnings("unchecked")
			List<Appointment> appos = readObject(List.class);
			data.clear();
			data.setAppos(appos);
			toJavaFx.onPush();
		}
	}
	
	/**
	 * Implements logic which is associated with sending messages to the server.
	 * 
	 * @author Jan Mothes
	 */
	public static class ClientProtocolService extends ProtocolWriter {
		
		private final SocketSide client;
		private final ClientData data;
		
		public ClientProtocolService(Client client, OutputStream outputStream) throws IOException {
			super(client, outputStream);
			this.client = client;
			data = ClientData.getInstance();
		}
		
		/*
		 * [CONNECTED]
		 * ----------------------------------------------------------
		 */
		public void login(String username, String password) {
			send(ProtocolState.LOGIN_ATEMPT, username, password);
		}
		
		public synchronized void close() throws IOException {
			send(ProtocolState.CLOSE);
			client.shutdown();
		}

		/*
		 * [LOGGED_IN]
		 * ----------------------------------------------------------
		 */
		
		public void getAppos() {
			send(ProtocolState.GET_APPOS);
		}
		
		public void newAppo(Appointment appo) {
			send(ProtocolState.NEW_APPO, appo);
		}
		
		public void setAppo(Appointment appo) {
			send(ProtocolState.SET_APPO, appo);
			
		}

		public void deleteAppo(Appointment appo) {
			send(ProtocolState.DEL_APPO, appo);
		}
		
		public synchronized void logout() {
			send(ProtocolState.LOGOUT);
			client.setState(ProtocolState.CONNECTED);
			data.clear();
			
		}
	}
}
