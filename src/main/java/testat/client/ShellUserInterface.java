package testat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import testat.shared.TablePrinter;
import testat.shared.model.Appointment;
import testat.shared.net.ProtocolState;

public class ShellUserInterface implements Runnable {

	private final Client client;
	private final ClientData data;
	private final BufferedReader br = 
			new BufferedReader(new InputStreamReader(System.in));
	
	public ShellUserInterface(Client client) {
		this.client = client;
		this.data = ClientData.getInstance();
	}
	
	@Override
	public void run() {
		System.out.println(client.getSideName() + ": Started command line UI thread");
		while(client.isRunning()) {
			System.out.println("Enter action:");
			String action;
			try {
				action = br.readLine();
				boolean canPrint = action.equals("print")
						&& client.getState().equals(ProtocolState.LOGGED_IN);
				if (client.getState().isActionAllowed(action) || canPrint) {
					switch(action) {
					
					// [CONNECTED]
					case ProtocolState.LOGIN_ATEMPT:
						System.out.println("Username:");
						String username = br.readLine();
						System.out.println("Password:");
						String password = br.readLine();
						client.getProtocolService().login(username, password);
						break;
					case ProtocolState.CLOSE:
						client.getProtocolService().close();
						break;
					
					// [LOGGED_IN]
					case ProtocolState.GET_APPOS:
						client.getProtocolService().getAppos();
						break;
					case ProtocolState.NEW_APPO:
						Appointment appo = new Appointment("newTestAppo");
						client.getProtocolService().newAppo(appo);
						break;
					case ProtocolState.SET_APPO:
						System.out.println("ID:");
						String id = br.readLine();
						System.out.println("New name:");
						String name = br.readLine();
						Appointment appo2 = data.get(Long.parseLong(id));
						appo2.setName(name);
						client.getProtocolService().setAppo(appo2);
						break;
					case ProtocolState.DEL_APPO:
						System.out.println("ID:");
						String id2 = br.readLine();
						Appointment appo3 = data.get(Long.parseLong(id2));
						client.getProtocolService().deleteAppo(appo3);
						break;
					case ProtocolState.LOGOUT:
						client.getProtocolService().logout();
						break;
					case "print":
						print();
						break;
					}
				} else {
					System.out.println("Not allowed!");
				}
			} catch (IOException e) {e.printStackTrace();}
		}
		System.out.println(client.getSideName() + ": Command line UI thread ended");
	}
	
	private void print() {
		String[] head = {"ID", "Name", "Desc", "Start"};
		final TablePrinter table = new TablePrinter().addRow(head).addRow(new String[0]);
		for (Appointment a : data.getAppos()) {
			table.addRow(a.toStringArray());
		}
		System.out.print(table);
	}
}
