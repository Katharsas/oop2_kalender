package testat.client;

import java.util.HashSet;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import testat.shared.model.Appointment;

public class ClientData {

	private static ClientData instance = new ClientData();
	public static ClientData getInstance() {return instance;}
	
	private HashSet<Appointment> appos = new HashSet<>();
	
	private ClientData() {}
	
	public synchronized HashSet<Appointment> getAppos() {
		return new HashSet<>(appos);
	}
	
	public synchronized ObservableList<Appointment> getApposForJavaFx() {
		return FXCollections.observableArrayList(new HashSet<>(appos));
	}
	
	/**
	 * Adds to / overwrites existing appos.
	 */
	public synchronized void setAppos(Iterable<? extends Appointment> appos) {
		for(Appointment appo : appos) {
			boolean added = this.appos.add(appo);
			if (!added) {
				this.appos.remove(appo);
				this.appos.add(appo);
			}
		}
	}
	
	public synchronized void clear() {
		appos.clear();
	}
	
	public synchronized Appointment get(long id) {
		return Appointment.getFromId(appos, id);
	}
}
