package testat.client.gui;

import java.net.URL;
import java.util.ResourceBundle;

import testat.client.Client;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginForm implements Initializable {
	
	@FXML
	private TextField username;
	
	@FXML
	private PasswordField password;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	@FXML
	private void onLogin() {
		Client.getInstance().getProtocolService()
				.login(username.getText(), password.getText());
	}
	
}
