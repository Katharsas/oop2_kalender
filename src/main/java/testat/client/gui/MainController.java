package testat.client.gui;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.function.Function;

import testat.client.Client;
import testat.client.Client.ClientProtocolService;
import testat.shared.model.Appointment;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class MainController implements Initializable {

	private JavaFxGui gui;
	private ClientProtocolService service;
	
	private Appointment selectedAppo = null;
	
	@FXML
	private AnchorPane viewWrapper;
	
	@FXML
	private Button buttonNew;
	
	@FXML
	private Button buttonEdit;
	
	@FXML
	private Button buttonDelete;
	
	@FXML
	private ComboBox<View> selectView;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		gui = JavaFxGui.getInstance();
		service = Client.getInstance().getProtocolService();
		selectView.setConverter(new ToString<>(view -> view.toString()));
		selectView.setItems(
				FXCollections.observableList(Arrays.asList(View.values())));
		selectView.valueProperty().addListener(
				(observable, oldValue, newValue) -> {
					if (newValue.equals(View.TABLE)) gui.showTableView();
					if (newValue.equals(View.WEEK)) gui.showCalendarView();
				});
		
		
		buttonNew.addEventHandler(ActionEvent.ACTION, e -> {
			showAppointmentForm(true);
		});
		buttonEdit.addEventHandler(ActionEvent.ACTION, e -> {
			showAppointmentForm(false);
		});
		buttonDelete.addEventHandler(ActionEvent.ACTION, e -> {
			if (selectedAppo != null) {
				service.deleteAppo(selectedAppo);
				service.getAppos();
			}
		});
	}
	
	public void initView() {
		selectView.setValue(View.TABLE);
	}
	
	private void showAppointmentForm(boolean newAppo) {
		Stage dialog = gui.showAppoDialog();
		dialog.setTitle((newAppo ? "Create New " : "Edit") + " Appointment" );
		AppointmentController controller = gui.getAppoController();
		controller.setDialogStage(dialog);
		if (newAppo) {
			controller.newAppo();
			dialog.showAndWait();
			if (controller.isConfirmed()) {
				service.newAppo(controller.getAppo());
				service.getAppos();
			}
		}
		else if (selectedAppo != null) {
			controller.setAppo(selectedAppo);
			dialog.showAndWait();
			if (controller.isConfirmed()) {
				service.setAppo(controller.getAppo());
				service.getAppos();
			}
		}
	}
	
	public void update() {
		if (gui.getTableController() != null) gui.getTableController().update();
		if (gui.getCalendarController() != null) gui.getCalendarController().update();
	}
	
	public void setSelectedAppo(Appointment appo) {
		this.selectedAppo = appo;
	}
	
	private static class ToString<T> extends StringConverter<T> {
		Function<T, String> toString;
		public ToString(Function<T, String> toString) {
			this.toString = toString;
		}
		@Override
		public String toString(T object) {
			return toString.apply(object);
		}
		@Override
		public T fromString(String string) {
			throw new UnsupportedOperationException();
		}
	}
	
	private static enum View {
		TABLE("Table View"),
		WEEK("Weekly View"),
		MONTH("Monthly View");
		
		private String name;
		private View(String name) {
			this.name = name;
		}
		@Override
		public String toString() {
			return name;
		}
	}
}
