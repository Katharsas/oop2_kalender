package testat.client.gui;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import testat.client.ClientData;
import testat.shared.model.Appointment;
import testat.shared.model.Date;
import testat.shared.model.DateTime;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class CalenderCanvas extends ResizableCanvas {

	private final ClientData data = ClientData.getInstance();
	private DateTime weekStart;
	private DateTime weekEnd;
	private Map<Appointment, Boolean> apposToVisible;
	private List<Appointment> appos;
	
	private final static Color GRID_COLOR = new Color(0.7, 0.7, 0.7, 1);
	private final static Color APPO_COLOR = new Color(0.2, 0.2, 0.2, 1);
	
	private final static int ROW_HEIGHT = 40;
	private final static int ROW_INSETS = 7;
	
	private final static int APPO_WIDTH_SWITCH = 50;
	private final static int APPO_MIN_WIDTH = 10;
	
	private final static int ROW_HEAD_HEIGHT = 20;
	
	public CalenderCanvas() {
		apposToVisible = new HashMap<>();
		updateWeek(weekStart, weekEnd);
	}
	
	public void updateWeek(DateTime weekStart, DateTime weekEnd) {
		this.weekStart = weekStart;
		this.weekEnd = weekEnd;
		updateData();
	}
	
	public void updateData() {
		appos = new LinkedList<Appointment>(data.getAppos());
		appos.sort((appo1, appo2) -> appo1.getDateStart().compareTo(appo2.getDateStart()));
		apposToVisible.clear();
		if (weekStart != null && weekEnd != null) {
			for (Appointment appo : appos) {
				Date start = appo.getDateStart();
				Date end = appo.getDateEndEstimate();
				boolean visible = weekStart.isBefore(end) && start.isBefore(weekEnd);
				apposToVisible.put(appo, visible);
			}
			setHeight(ROW_HEIGHT * data.getAppos().size() + ROW_HEAD_HEIGHT);
			draw();
		}
	}
	
	@Override
	public void draw() {
		
		double width = getWidth();
		double height = getHeight();
		
		GraphicsContext gc = getGraphicsContext2D();
		
		gc.setFill(new Color(0.95, 0.95, 0.95, 1));
		gc.fillRect(0, 0, width, height);
		
		// ---------------------------------------------------  Columns
		gc.setStroke(GRID_COLOR);
		
		double columnWidth = width / 7;
		double currentWidth = columnWidth;
		for (int i = 0; i < 6; i++) {
			gc.strokeLine(currentWidth, 0, currentWidth, height);
			currentWidth += columnWidth;
		}
		// ---------------------------------------------------  Rows / Header
		currentWidth = 0;
		int currentHeight = 0;
		
		gc.setStroke(Color.web("777"));
		String[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
		for (int i = 0; i < 7; i++) {
			gc.strokeText(daysOfWeek[i], currentWidth + 5, currentHeight + 14);
			currentWidth += columnWidth;
		}
		currentHeight += ROW_HEAD_HEIGHT;
		
		gc.setStroke(GRID_COLOR);
		gc.strokeLine(0, currentHeight, width, currentHeight);
		
		// ---------------------------------------------------  Appoinment Rows
		int appoHeight = ROW_HEIGHT - (2 * ROW_INSETS);
		
		for (Appointment appo : appos) {
			currentHeight += ROW_INSETS;

			if(apposToVisible.get(appo)) {
				currentHeight = drawAppo(gc, appo, currentHeight, columnWidth, appoHeight);
			} else currentHeight += appoHeight;
			
			gc.setStroke(GRID_COLOR);
			currentHeight += ROW_INSETS;
			gc.strokeLine(0, currentHeight, width, currentHeight);
		}
	}
	
	private int drawAppo(GraphicsContext gc, Appointment appo,
			int currentHeight, double columnWidth, int appoHeight) {
		
		gc.setStroke(APPO_COLOR);
		
		DateTime dateStart = appo.getDateStart();
		DateTime dateEnd = appo.getDateEndEstimate();
		double start = DateTime.getDifferenceInDaysExact(weekStart, dateStart) * columnWidth;
		double end = DateTime.getDifferenceInDaysExact(weekStart, dateEnd) * columnWidth;
		
		double appoWidth = (appo.hasEnd() ?
				Math.max(end - start, APPO_MIN_WIDTH) : columnWidth);
		
		int currentAppoHeight = appoHeight;
		int currentHeightText = currentHeight + 11;
		int textInsetWidth = 3;
		
		if (appoWidth < APPO_WIDTH_SWITCH) {
			currentAppoHeight = appoHeight - 14;
			currentHeight += 14;
			textInsetWidth = -3;
		}
		
		gc.setFill(Color.web("E69D20"));
		gc.fillRect(start, currentHeight, appoWidth, currentAppoHeight);
		gc.strokeRect(start, currentHeight, appoWidth, currentAppoHeight);
		gc.strokeText(appo.getName(), start + textInsetWidth, currentHeightText);
		
		currentHeight += currentAppoHeight;
		return currentHeight;
	}
}
