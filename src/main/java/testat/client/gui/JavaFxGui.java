package testat.client.gui;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PipedInputStream;

import testat.client.Client;
import testat.shared.net.ProtocolState;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * JavaFx graphical user interface.
 * 
 * @author Jan Mothes
 */
public class JavaFxGui extends Application {

	private static JavaFxGui instance;
	public static JavaFxGui getInstance() {return instance;}
	public JavaFxGui() {instance = this;}
	
	private static final String loginPagePath = "loginPage.fxml";
	private static final String mainPagePath = "mainPage.fxml";
	private static final String tableViewPath = "tableView.fxml";
	private static final String calendarViewPath = "weekView.fxml";
	
	private static final String appoFormPath = "appoForm.fxml";
	
	private Client client;
	private ClientListener clientListener;
	
	private Stage primaryStage;
	private AnchorPane login;
	private AnchorPane main;
	
	private TableController tableController;
	private CalendarController calendarController;
	private MainController mainController;
	private AppointmentController appoController;
	
	public TableController getTableController() {
		return tableController;
	}
	
	public CalendarController getCalendarController() {
		return calendarController;
	}
	
	public MainController getMainController() {
		return mainController;
	}
	
	public AppointmentController getAppoController() {
		return appoController;
	}

	private void initLoginLayout() {
		login = loadFxml(AnchorPane.class, loginPagePath);

		Scene scene = new Scene(login);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private void initMainLayout() {
		main = loadFxml(AnchorPane.class, mainPagePath);

		Scene scene = new Scene(main);
		primaryStage.setScene(scene);
		primaryStage.show();
		mainController.initView();
	}
	
	public void showTableView() {
		TableView<?> table = loadFxml(TableView.class, tableViewPath);
		
		Scene scene = primaryStage.getScene();
		AnchorPane viewWrapper = (AnchorPane) scene.lookup("#viewWrapper");
		addToAnchorPane(viewWrapper, table);
	}
	
	public void showCalendarView() {
		VBox vbox = loadFxml(VBox.class, calendarViewPath);
		Scene scene = primaryStage.getScene();
		AnchorPane viewWrapper = (AnchorPane) scene.lookup("#viewWrapper");
		addToAnchorPane(viewWrapper, vbox);
	}
	
	public Stage showAppoDialog() {
		AnchorPane appoForm = loadFxml(AnchorPane.class, appoFormPath);
		Scene scene = new Scene(appoForm);
		
		Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        dialogStage.setScene(scene);
        
		return dialogStage;
	}
	
	private void addToAnchorPane(AnchorPane parent, Node child) {
		AnchorPane.setBottomAnchor(child, 0d);
		AnchorPane.setTopAnchor(child, 0d);
		AnchorPane.setLeftAnchor(child, 0d);
		AnchorPane.setRightAnchor(child, 0d);
		parent.getChildren().clear();
		parent.getChildren().add(child);
	}
	
	private <T> T loadFxml(Class<T> clazz, String fileName) {
		final FXMLLoader loader = new FXMLLoader();;
		loader.setLocation(JavaFxGui.class.getResource("fxml/" + fileName));
		T node = null;
		try {
			node = loader.load();
		} catch (IOException e) {
			System.out.println("Loading view " + fileName + " failed!");
			e.printStackTrace();
		}
		if (fileName.equals(tableViewPath)) {
			tableController = loader.getController();
		} else if(fileName.equals(mainPagePath)) {
			mainController = loader.getController();
		} else if(fileName.equals(appoFormPath)) {
			appoController = loader.getController();
		} else if(fileName.equals(calendarViewPath)) {
			calendarController = loader.getController();
		}
		return clazz.cast(node);
	}
	
	@Override
	public void init() throws Exception {
		client = Client.getInstance();
		clientListener = new ClientListener(client, this);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Appointment Manager");
		
		initLoginLayout();
		
		Service<Void> service = new Service<Void>() {
			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws Exception {
						clientListener.run();
						return null;
					}
				};
			}
		};
		service.start();
	}
	
	@Override
	public void stop() throws Exception {
		if (client.getState().equals(ProtocolState.LOGGED_IN)) {
			client.getProtocolService().logout();
		}
		client.getProtocolService().close();
	}
	
	public static class ClientListener implements Runnable {
		
		private final Client client;
		private final JavaFxGui gui;
		private final ObjectInputStream fromClient;
		
		public ClientListener(Client client, JavaFxGui gui) throws IOException {
			this.gui = gui;
			this.client = client;
			fromClient = new ObjectInputStream(new PipedInputStream(client.getJavaFxPipe()));
		}
		@Override
		public void run() {
			while(client.isRunning()) {
				try {
					String message = (String) fromClient.readObject();
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							switch (message) {
							case ProtocolState.LOGIN_SUCCESSFUL:
								gui.initMainLayout();
								client.getProtocolService().getAppos();
							case ProtocolState.LOGIN_FAILED:
								break;
							case ProtocolState.CLOSE:
								break;
							case ProtocolState.PUSH_APPOS:
								gui.mainController.update();
								break;
							case ProtocolState.LOGOUT:
								gui.initLoginLayout();
								break;
							}
						}
					});
				} catch (IOException | ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
			try {
				fromClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
