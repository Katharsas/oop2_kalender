package testat.client.gui;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import testat.shared.model.Appointment;
import testat.shared.model.DateTime;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AppointmentController implements Initializable {

	@FXML
	private Label labelType;

	@FXML
	private TextField name;

	@FXML
	private TextArea description;

	@FXML
	private DatePicker dateStart;

	@FXML
	private TextField dateStartHour;

	@FXML
	private TextField dateStartMinute;

	@FXML
	private CheckBox allDay;

	@FXML
	private DatePicker dateEnd;

	@FXML
	private TextField dateEndHour;

	@FXML
	private TextField dateEndMinute;
	
	@FXML
	private Button buttonCancel;
	
	@FXML
	private Button buttonOk;

	
	private Stage dialogStage;
	private Appointment appo = null;
	private boolean confirmed = false;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	private void initForm() {
		appoToForm(appo);
		
		buttonCancel.addEventHandler(ActionEvent.ACTION, event -> {
			dialogStage.close();
		});
		buttonOk.addEventHandler(ActionEvent.ACTION, event -> {
			try {
				appo = appoFromForm(appo);
				confirmed = true;
				dialogStage.close();
			} catch(IllegalArgumentException e) {
				System.err.println("Check appointment form for illegal input!");
				e.printStackTrace();
			}
		});
	}
	
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setAppo(Appointment appo) {
		this.appo = appo;
		labelType.setText("Edit Appointment");
		initForm();
	}

	public void newAppo() {
		this.appo = new Appointment();
		labelType.setText("Create new Appointment");
		initForm();
	}
	
	public Appointment getAppo() {
		return appo;
	}
	
	public boolean isConfirmed() {
		return confirmed;
	}
	
	private void appoToForm(Appointment appo) {
		name.setText(appo.getName());
		description.setText(appo.getDescription());
		DateTime date;
		
		date = appo.getDateStart();
		dateStart.setValue(LocalDate.of(
				date.getYear(), date.getMonthOfYear(), date.getDayOfMonth()));
		dateStartHour.setText("" + date.getHour());
		dateStartMinute.setText("" + date.getMinute());
		
		date = appo.getDateEnd();
		dateEnd.setValue(LocalDate.of(
				date.getYear(), date.getMonthOfYear(), date.getDayOfMonth()));
		dateEndHour.setText("" + date.getHour());
		dateEndMinute.setText("" + date.getMinute());
		
		allDay.setSelected(!appo.hasEnd());
	}
	
	private Appointment appoFromForm(Appointment appo) {
		appo.setName(name.getText());
		appo.setDescription(description.getText());
		LocalDate picker;
		
		picker = dateStart.getValue();
		DateTime start = new DateTime(
				picker.getYear(), picker.getMonthValue(), picker.getDayOfMonth(),
				Integer.parseInt(dateStartHour.getText()),
				Integer.parseInt(dateStartMinute.getText()));
		
		picker = dateEnd.getValue();
		DateTime end = new DateTime(
				picker.getYear(), picker.getMonthValue(), picker.getDayOfMonth(),
				Integer.parseInt(dateEndHour.getText()),
				Integer.parseInt(dateEndMinute.getText()));
		
		appo.setDate(start, end);
		appo.setHasEnd(!allDay.isSelected());
		return appo;
	}
}
