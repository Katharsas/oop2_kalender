package testat.client.gui;

import java.net.URL;
import java.util.ResourceBundle;

import testat.shared.model.Date;
import testat.shared.model.DateTime;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;

public class CalendarController implements Initializable {
	
	private Date week = DateTime.now();
	private CalenderCanvas canvas;
	
	@FXML
	private Button buttonPrev;
	
	@FXML
	private Button buttonNext;
	
	@FXML
	private Label label;
	
	@FXML
	private ScrollPane canvasContainer;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		canvas = new CalenderCanvas();
		Pane canvasPane = new Pane();
		
		canvas.widthProperty().bind(canvasPane.widthProperty());
		canvasPane.prefHeightProperty().bind(canvas.heightProperty());
		
		canvasPane.getChildren().add(canvas);
		
		canvasContainer.setFitToWidth(true);
		canvasContainer.setContent(canvasPane);
		
		buttonPrev.addEventHandler(ActionEvent.ACTION, event -> {
			week = Date.getDateFromDifference(week, -7);
			updateWeek();
		});
		buttonNext.addEventHandler(ActionEvent.ACTION, event -> {
			week = Date.getDateFromDifference(week, 7);
			updateWeek();
		});
		updateWeek();
	}
	
	public void update() {
		canvas.updateData();
	}

	public void updateWeek() {
		label.setText("Week " + week.getWeekOfYear() + ", " + week.getYearOfWeek());
		
		DateTime weekStart = new DateTime(Date.roundToStartOfWeek(week));
		DateTime weekEnd = new DateTime(Date.roundToEndOfWeek(week));
		canvas.updateWeek(weekStart, weekEnd);
	}
}
