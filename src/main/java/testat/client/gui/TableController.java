package testat.client.gui;

import java.net.URL;
import java.util.ResourceBundle;

import testat.client.ClientData;
import testat.shared.model.Appointment;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class TableController implements Initializable {
	
	private JavaFxGui gui;
	private ClientData data = ClientData.getInstance();
	
	@FXML
	private TableView<Appointment> table;
	
	@FXML
	private TableColumn<Appointment, String> tableColumnName;
	
	@FXML
	private TableColumn<Appointment, String> tableColumnDescription;
	
	@FXML
	private TableColumn<Appointment, String> tableColumnDateStart;
	
	@FXML
	private TableColumn<Appointment, String> tableColumnDateEnd;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		gui = JavaFxGui.getInstance();
		
		tableColumnName.setCellValueFactory(
				new PropertyValueFactory<Appointment, String>("name"));
		
		tableColumnDescription.setCellValueFactory(
				new PropertyValueFactory<Appointment, String>("description"));
		
		tableColumnDateStart.setCellValueFactory(p ->
				new ReadOnlyObjectWrapper<String>(p.getValue().getDateStart().toString()));
		
		tableColumnDateEnd.setCellValueFactory(p -> {
				Appointment appo = p.getValue();
				String result = appo.hasEnd() ? appo.getDateEnd().toString() : "all day";
				return new ReadOnlyObjectWrapper<String>(result);
		});
		table.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue)
						-> gui.getMainController().setSelectedAppo(newValue));
		
		update();
	}
	
	public void update() {
		// this is an ugly hack needed because JavaFX refresh bug
		// http://stackoverflow.com/questions/11065140/javafx-2-1-tableview-refresh-items
		table.getItems().removeAll(data.getApposForJavaFx());
		
		table.setItems(data.getApposForJavaFx());
	}
}
