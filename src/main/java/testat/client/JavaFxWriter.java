package testat.client;

import java.io.IOException;
import java.io.OutputStream;

import testat.shared.net.ProtocolState;
import testat.shared.net.ProtocolWriter;
import testat.shared.net.SocketSide;

public class JavaFxWriter extends ProtocolWriter {

	public JavaFxWriter(SocketSide side, OutputStream outputStream) throws IOException {
		super(side, outputStream);
	}
	
	@Override
	protected String getTarget() {
		return "JavaFX gui thread";
	}

	public void onLoginSuccess() {
		send(ProtocolState.LOGIN_SUCCESSFUL);
	}
	
	public void onLoginFailed() {
		send(ProtocolState.LOGIN_FAILED);
	}
	
	public void close() {
		send(ProtocolState.CLOSE);
	}
	
	public void onPush() {
		send(ProtocolState.PUSH_APPOS);
	}
	
	public void logout() {
		send(ProtocolState.LOGOUT);
	}
}
