package testat.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import testat.shared.model.Appointment;
import testat.shared.net.ProtocolListener;
import testat.shared.net.ProtocolState;
import testat.shared.net.SocketConnection;
import testat.shared.net.SocketSide;
import testat.shared.net.ProtocolWriter;
import testat.shared.net.ProtocolState.ProtocolViolationException;

public class Server extends SocketSide {
	
	private static boolean running = true;
	
	public static void main(String[] args) throws IOException {
		System.out.println("Listening for connections...");
		try (ServerSocket serverSocket = new ServerSocket(SocketConnection.PORT);) {
			while(running) {
				Socket socket = serverSocket.accept();
				SocketSide socketSide = new Server(socket);
				Thread socketThread = new Thread(socketSide);
				socketThread.start();
			}
		}
	}
	
	private static final boolean AUTO_LOGIN = false;
	
	private User currentUser = null;
	private ServerProtocolService writer;
	
	public Server(Socket socket) throws IOException {
		super(socket);
		if(AUTO_LOGIN) {
			System.out.println(getSideName() + ": Auto-Login is enabled");
			currentUser = ServerData.getInstance().authenticate("admin", "admin");
			writer.onLogin(ProtocolState.LOGIN_SUCCESSFUL);
		}
	}
	
	@Override
	protected ProtocolListener createProtocolInput(InputStream inputStream)
			throws IOException {
		return new ServerProtocolListener(this, inputStream);
	}
	
	@Override
	protected ProtocolWriter createProtocolOutput(OutputStream outputStream)
			throws IOException {
		writer = new ServerProtocolService(this, outputStream);
		return writer;
	}
	
	@Override
	public boolean isServer() {
		return true;
	}
	
	public synchronized void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
	
	public synchronized User getCurrentUser() {
		return currentUser;
	}
	
	public ServerProtocolService getWriter() {
		return writer;
	}
	
	public void notifyDataChangeForUser(User user) {
		if(user.equals(currentUser)) {
			try {
				writer.push();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Handles all actions triggered by client messages.
	 * 
	 * @author Jan Mothes
	 */
	public static class ServerProtocolListener extends ProtocolListener {

		private final Server server;
		private final ServerData data;
		
		public ServerProtocolListener(Server server, InputStream inputStream) throws IOException {
			super(server, inputStream);
			this.server = server;
			data = ServerData.getInstance();
		}
		
		@Override
		protected void onMessageReceived(String message) throws ProtocolViolationException, IOException {
			switch(message) {
			
			// [CONNECTED]
			case ProtocolState.LOGIN_ATEMPT:
				onLoginAtempt();
				break;
			case ProtocolState.CLOSE:
				onClose();
				break;
				
			// [LOGGED_IN]
			case ProtocolState.GET_APPOS:
				onGetAppos();
				break;
			case ProtocolState.NEW_APPO:
				onNewAppo();
				break;
			case ProtocolState.SET_APPO:
				onSetAppo();
				break;
			case ProtocolState.DEL_APPO:
				onDelAppo();
				break;
			case ProtocolState.LOGOUT:
				onLogout();
				break;
			
			default: throwIllegalProtocolMessageException(message);
			}
		}

		/*
		 * [CONNECTED]
		 * ----------------------------------------------------------
		 */
		
		private void onLoginAtempt() throws ProtocolViolationException, IOException {
			String username = readObject(String.class);
			String password = readObject(String.class);
			User user = data.authenticate(username, password);
			boolean success = user != null;
			if (success) server.setCurrentUser(user);
			server.getWriter().onLogin(success ? ProtocolState.LOGIN_SUCCESSFUL :
				ProtocolState.LOGIN_FAILED);
		}
		
		private void onClose() throws IOException {
			server.getWriter().close();
		}

		/*
		 * [LOGGED_IN]
		 * ----------------------------------------------------------
		 */

		private void onGetAppos() throws IOException {
			server.getWriter().push();
		}
		
		private void onNewAppo() throws ProtocolViolationException, IOException{
			Appointment appo = readObject(Appointment.class);
			data.addNewAppointment(server.getCurrentUser(), appo);
		}

		private void onSetAppo() throws ProtocolViolationException, IOException {
			Appointment appo = readObject(Appointment.class);
			data.setAppointment(server.getCurrentUser(), appo);
		}

		private void onDelAppo() throws ProtocolViolationException, IOException {
			Appointment appo = readObject(Appointment.class);
			data.deleteAppointment(server.getCurrentUser(), appo);
		}
		
		private void onLogout() throws IOException {
			data.unregister(server);
			server.setCurrentUser(null);
			server.setState(ProtocolState.CONNECTED);
		}
	}
	
	/**
	 * Executes protocol state changes and sends all messages/data to client.
	 * 
	 * @author Jan Mothes
	 */
	public static class ServerProtocolService extends ProtocolWriter {
		
		private final ServerData data;
		private final Server server;
		
		public ServerProtocolService(Server server, OutputStream outputStream) throws IOException {
			super(server, outputStream);
			this.server = server;
			data = ServerData.getInstance();
		}

		/*
		 * [CONNECTED]
		 * ----------------------------------------------------------
		 */
		
		public synchronized void onLogin(String answer) throws IOException {
			send(answer);
			if (answer.equals(ProtocolState.LOGIN_SUCCESSFUL)) {
				server.setState(ProtocolState.LOGGED_IN);
				data.registerForDataChange(server);
			}
		}
		
		public synchronized void close() throws IOException {
			send(ProtocolState.CLOSE);// echo close so that client listener can end
			server.shutdown();
		}

		/*
		 * [LOGGED_IN]
		 * ----------------------------------------------------------
		 */
		
		public void push() throws IOException {
			send(ProtocolState.PUSH_APPOS, data.getAppos(server.getCurrentUser()));
		}
	}
}
