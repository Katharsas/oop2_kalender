package testat.server;

import java.io.Serializable;
import java.util.Objects;

/**
 * Immutable user.
 * 
 * @author Jan Mothes
 */
public class User implements Serializable {

	private static final long serialVersionUID = -8393602204009332040L;
	
	private final String username;
	private final String password;
	
	public User(String username, String password) {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}

	@Override
	public int hashCode() {
		return username.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		User other = (User) obj;
		return username.equals(other.username);
	}
}
