package testat.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import testat.shared.model.Appointment;

public class ServerData {

	private static ServerData instance = new ServerData();
	public static ServerData getInstance() {return instance;}
	
	private final Path usersPath = Paths.get("users.save");
	
	private Set<User> users = new HashSet<>();
	private HashMap<User, Set<Appointment>> usersToAppos = new HashMap<>();
	private Set<Server> registered = new HashSet<>();
	
	private ServerData() {
		File usersFile = usersPath.toFile();
		boolean loaded = false;
		if(usersFile.exists() && usersFile.isFile()) {
			loaded = loadData();
		}
		if (!loaded) {
			User user = new User("admin", "admin");
			Set<Appointment> appos = new HashSet<>();
			appos.add(new Appointment("test1"));
			appos.add(new Appointment("test2"));
			appos.add(new Appointment("test3"));
			users.add(user);
			usersToAppos.put(user, appos);
			saveData();
		}
	}
	
	public User authenticate(String username, String password) {
		for (User user : users) {
			if (user.getUsername().equals(username)) {
				return user.getPassword().equals(password) ? user : null;
			}
		}
		return null;
	}

	public List<Appointment> getAppos(User user) {
		synchronized (usersToAppos) {
			return new LinkedList<Appointment>(usersToAppos.get(user));
		}
	}
	
	public void addNewAppointment(User user, Appointment appo) {
		appo.updateId();
		synchronized (usersToAppos) {
			boolean added = usersToAppos.get(user).add(appo);
			if (!added) throw new IllegalStateException(
					"ID Error! ID is not new, cannot add: " + appo);
		}
		saveAppos(user);
		notifyDataChange(user);
	}
	
	public void setAppointment(User user, Appointment appo) {
		synchronized (usersToAppos) {
			usersToAppos.get(user).remove(appo);
			usersToAppos.get(user).add(appo);
		}
		saveAppos(user);
		notifyDataChange(user);
	}
	
	public void deleteAppointment(User user, Appointment appo) {
		synchronized (usersToAppos) {
			usersToAppos.get(user).remove(appo);
		}
		saveAppos(user);
		notifyDataChange(user);
	}
	
	private synchronized void saveData() {
		System.out.println("Saving all data to files.");
		writeObjectToFile(usersPath, users);
		for(User user : users) {
			saveAppos(user);
		}
	}
	
	private synchronized boolean saveAppos(User user) {
		Set<Appointment> appos = usersToAppos.get(user);
		Path apposPath = Paths.get(user.getUsername() + ".save");
		return writeObjectToFile(apposPath, appos);
	}
	
	private synchronized boolean loadData() {
		System.out.println("Loading all data from files.");
		@SuppressWarnings("unchecked")
		Set<User> users = (Set<User>) readObjectFromFile(usersPath);
		if (users == null) return false;
		for (User user : users) {
			Path apposPath = Paths.get(user.getUsername() + ".save");
			@SuppressWarnings("unchecked")
			Set<Appointment> appos = (Set<Appointment>) readObjectFromFile(apposPath);
			if (appos == null) return false;
			Appointment.onLoad(appos);
			usersToAppos.put(user, appos);
		}
		this.users = users;
		return true;
	}
	
	private synchronized boolean writeObjectToFile(Path path, Object object) {
		try(	FileOutputStream fout = new FileOutputStream(path.toFile());
				ObjectOutputStream oos = new ObjectOutputStream(fout);
				) {
			oos.writeObject(object);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private synchronized Object readObjectFromFile(Path path) {
		try(	FileInputStream fin = new FileInputStream(path.toFile());
				ObjectInputStream ois = new ObjectInputStream(fin);
				) {
			return ois.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private void notifyDataChange(User user) {
		System.out.println("Data change for user "+user.getUsername());
		for(Server server : registered) {
			System.out.println("Notifying server!");
			server.notifyDataChangeForUser(user);
		}
	}
	
	public synchronized void registerForDataChange(Server server) {
		registered.add(server);
	}
	
	public synchronized void unregister(Server server) {
		registered.remove(server);
	}
}
