package testat.shared.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.SocketException;

import testat.shared.net.ProtocolState.ProtocolViolationException;

/**
 * Listening part of a socket duplex connection.
 * 
 * @author Jan Mothes
 */
public abstract class ProtocolListener implements Runnable {
	
	private final SocketSide side;
	private final ObjectInputStream inputStream;
	
	public ProtocolListener(SocketSide side, InputStream inputStream) throws IOException {
		this.side = side;
		this.inputStream = new ObjectInputStream(inputStream);
	}
	
	@Override
	public void run() {
		System.out.println(side.getSideName() + ": Started listener thread");
		while(side.isRunning()) {
			String receivedMessage = null;
			try {
				receivedMessage = (String) inputStream.readObject();
			}
			catch (SocketException e) {
				side.shutdown();
			} catch (IOException | ClassNotFoundException e) {
				try {
					throw new ProtocolViolationException(side.getSideName()
							+ ": Receiving message from "
							+ side.getOtherSideName() + " failed!", e);
				} catch (ProtocolViolationException outerE) {
					e.printStackTrace();
				}
			}
			if (receivedMessage != null) {
				try {
					if (!side.getState().isActionAllowed(receivedMessage)) {
						throwIllegalProtocolMessageException(receivedMessage);
					}	
					onMessageReceived(receivedMessage);
				} catch (ProtocolViolationException | IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println(side.getSideName() + ": Listener thread ended");
	}
	
	protected <T> T readObject(Class<T> clazz) throws ProtocolViolationException {
		try {
			T receivedObject = clazz.cast(inputStream.readObject());
			return receivedObject;
		} catch (IOException | ClassNotFoundException e) {
			throw new ProtocolViolationException(side.getSideName()
					+ ": Receiving data from "
					+ side.getOtherSideName() + " failed!", e);
		}
	}
	
	protected abstract void onMessageReceived(String message) throws ProtocolViolationException, IOException;
	
	protected void throwIllegalProtocolMessageException(String message) throws ProtocolViolationException {
		throw new ProtocolViolationException(side.getSideName()
				+ ": Received message '" + message
				+ "' is not allowed in current state '" + side.getState() + "'!");
	}
	
	protected void waitUntilNotified() {
		while(true) {
			try {
				wait();
				break;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
