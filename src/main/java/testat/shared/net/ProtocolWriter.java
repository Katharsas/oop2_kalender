package testat.shared.net;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import testat.shared.net.ProtocolState.ProtocolViolationException;

/**
 * Writing part of a socket duplex connection.
 * 
 * @author Jan Mothes
 */
public class ProtocolWriter {

	private boolean available = false;
	
	private final SocketSide side;
	private final ObjectOutputStream outputStream;
	
	public ProtocolWriter(SocketSide side, OutputStream outputStream) {
		this.side = side;
		ObjectOutputStream temp = null;
		try {
			temp = new ObjectOutputStream(outputStream);
			available = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.outputStream = temp;
	}
	
	/**
	 * Sends a protocol message/action and any number of additional data objects
	 * to the other side.
	 */
	protected void send(String protocolAction, Object ... objects) {
		if (available) {
			if (!side.getState().isActionAllowed(protocolAction)) {
				try {
					throw new ProtocolViolationException("Action "
							+ protocolAction + " is not allowed in the current state!");
				} catch (ProtocolViolationException e) {e.printStackTrace();}
			}
			synchronized (outputStream) {
				try {
					outputStream.writeObject(protocolAction);
					for(Object object : objects) {
						outputStream.writeObject(object);
					}
				} catch (IOException e) {
					System.err.println(side.getSideName() + ": Sending data to "
							+ getTarget() + " failed!");
					e.printStackTrace();
				}
			}
		}
	}
	
	protected String getTarget() {
		return side.getOtherSideName();
	}
}
