package testat.shared.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Abstract super class for server-client and client main thread.
 * Manages creation of threads for duplex socket communication.
 * 
 * @author Jan Mothes
 */
public abstract class SocketSide implements Runnable {
	
	private boolean running = true;
	private final Socket socket;
	
	private final ProtocolListener listener;
	private final Thread listenerThread;
	
	private ProtocolState state = ProtocolState.CONNECTED;
	
	public SocketSide(Socket socket) throws IOException {
		this.socket = socket;
		System.out.println(getSideName() + ": Connected to " + getOtherSideName());
		
		//always create output first
		createProtocolOutput(socket.getOutputStream());
		listener = createProtocolInput(socket.getInputStream());
		listenerThread = new Thread(listener);
	}
	
	@Override
	public void run() {
		listenerThread.start();
		
		synchronized(this) {
			while(running) {
				try {
					wait();
					close();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Waits for all threads to exit, closes socket and then exits.
	 */
	private void close() {
		//continue any waiting threads to let them exit
		notifyListener();
		
		//Wait for threads
		try {
			listenerThread.join();
		} catch (InterruptedException e) {
			System.err.println("Waiting for socket listener failed! (ignored)");
			e.printStackTrace();
		}
		
		//give client side time to stop listening
		if(isServer()) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//Close socket
		try {
			synchronized(socket) {
				socket.close();
			}
		} catch (IOException e) {
			throw new IllegalStateException("Closing socket failed!");
		}
		System.out.println(getSideName() + ": Connection closed");
	}
	
	protected abstract ProtocolListener createProtocolInput(InputStream inputStream) throws IOException ;
	protected abstract ProtocolWriter createProtocolOutput(OutputStream outputStream) throws IOException ;
	
	public String getSideName() {
		return "    " + getSideName(isServer());
	}
	
	public String getOtherSideName() {
		return getSideName(!isServer());
	}
	
	protected abstract boolean isServer();
	
	private String getSideName(boolean isServer) {
		return isServer ? "server" : "client";
	}
	
	public ProtocolState getState() {
		synchronized (state) {
			return state;
		}
	}
	
	public void setState(ProtocolState state) {
		System.out.println(getSideName() + ": Protocol state changed to '" + state + "'!");
		synchronized(this.state) {
			this.state = state;
		}
	}
	
	public synchronized void notifyListener() {
		synchronized (listener) {
			listener.notify();
		}
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public synchronized void shutdown() {
		running = false;
		notify();
	}
}
