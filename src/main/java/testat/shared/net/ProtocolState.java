package testat.shared.net;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


/**
 * <pre>
 * Client connects to accepting server => [Connected]
 * 
 * [Connected]
 * 
 * 		Client: LOGIN_ATEMPT(Credentials) =>
 * 			Server: LOGIN_SUCCESSFUL
 * 			Server: LOGIN_FAILED
 * 
 * 		Server: LOGIN_SUCCESSFUL => [LoggedIn]
 * 		Server: LOGIN_FAILED
 * 		
 * 		Client: CLOSE => [Closing]
 * 
 * [LoggedIn]
 * 
 * 		Client: GET_APPOS =>
 * 			Server: PUSH_APPOS
 * 
 * 		Server: PUSH_APPOS(Appointment[])
 * 
 * 		Client: NEW_APPO(Appointment)
 * 		Client: SET_APPO(Appointment)
 * 		Client: DEL_APPO(Appointment)
 * 
 * 		Client: LOGOUT => [Connected]
 * 
 * 	[Closing] => Client & Server close socket
 * 
 * </pre>
 * 
 * 
 * 
 * @author Jan
 */
public enum ProtocolState {
	
	CONNECTED, LOGGED_IN;
	
	/*
	 * MESSAGES / ACTIONS
	 */
	
	//[Connected]
	public static final String LOGIN_ATEMPT = "login";
	public static final String LOGIN_SUCCESSFUL = "login_y";
	public static final String LOGIN_FAILED = "login_failed";
	public static final String CLOSE = "close";
	
	//[LoggedIn]
	public static final String GET_APPOS = "get";
	public static final String PUSH_APPOS = "push";
	
	public static final String NEW_APPO = "new";
	public static final String SET_APPO = "set";
	public static final String DEL_APPO = "del";
	
	public static final String LOGOUT = "logout";	

	
	public boolean isActionAllowed(String action) {
		return actionsAllowed(this).contains(action);
	}
	
	private static final List<String> whileConnected = Arrays.asList(new String[]
			{LOGIN_ATEMPT, LOGIN_SUCCESSFUL, LOGIN_FAILED, CLOSE});
	
	private static final List<String> whileLoggedIn = Arrays.asList(new String[]
			{GET_APPOS, PUSH_APPOS, NEW_APPO, SET_APPO, DEL_APPO, LOGOUT});
	
	private static List<String> actionsAllowed(ProtocolState state) {
		if (state.equals(CONNECTED)) return whileConnected;
		else if (state.equals(LOGGED_IN)) return whileLoggedIn;
		else return new LinkedList<>();
	}
	
	public static class ProtocolViolationException extends Exception {
		private static final long serialVersionUID = 8065642787302904697L;
		public ProtocolViolationException(String message) {
			super(message);
		}
		public ProtocolViolationException(String message, Throwable cause) {
			super(message, cause);
		}
	}
}
