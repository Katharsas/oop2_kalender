package testat.shared.model;

import java.io.Serializable;

/**
 * Represents an immutable date.
 * Date calculates everything on construction.
 * 
 * @author Jan Mothes
 */
public class Date implements Comparable<Date>, Serializable {
	
	/**
	 * Static ----------------------------------------
	 */
	
	/**
	 * Used by all date objects to calculate their day of week
	 */
	@SuppressWarnings("serial")
	private final static Date MONDAY = new Date(2014,10,13) {
		@Override protected int findDayOfWeek() {return 1;}
	};
	
	private static boolean isLeapYear(int year) {
		return year % 4 == 0 && ((!(year % 100 == 0)) || year % 400 == 0);
	}
	
	private static int getTotalDaysForMonth(int month, int year) {
		final boolean isLeapYear = isLeapYear(year);
		if (month==2) return isLeapYear ? 29 : 28;
		if (month < 8) return month % 2 == 0 ? 30 : 31;
		else return month % 2 == 0 ? 31 : 30;
	}
	
	private static int getTotalDaysForYear(int year) {
		int days = 0;
		for(int i = 1; i<= 12; i++) {
			days += getTotalDaysForMonth(i, year);
		}
		return days;
	}
	
	/**
	 * "from" date MUST be prior to "to" date
	 */
	protected static int getDifferenceInDays (Date from, Date to) {
		if (from.equals(to)) return 0;
		if (to.isBefore(from)) throw new IllegalArgumentException("nope");
		int days = 0;
		// days in years between
		for (int i = from.year+1; i < to.year; i++) {
			days += getTotalDaysForYear(i);
		}
		// days left in "from" year (until end of ear)
		for (int i = from.monthOfYear + 1; i <= 12; i++) {
			days += getTotalDaysForMonth(i, from.year);
		} 
		days += getTotalDaysForMonth(from.monthOfYear, from.year) - from.dayOfMonth;
		// days left in "to" year (from start of year)
		days += to.elapsedDaysForYear;
		if (from.year == to.year) days -= getTotalDaysForYear(from.year);
		
		return days;
	}
	
	public static Date getDateFromDifference(Date date, int days) {
		if (days == 0) return date;
		int dayOfMonthIndex = date.getDayOfMonth() - 1 + days;
		int year = date.getYear();
		int month = date.getMonthOfYear();
		
		int daysForMonth = Date.getTotalDaysForMonth(month, year);
		while (dayOfMonthIndex >= daysForMonth) {
			if (month == 12) {
				year++;
				month = 0;
			}
			dayOfMonthIndex -= daysForMonth;
			month++;
			daysForMonth = Date.getTotalDaysForMonth(month, year);
		}
		while (dayOfMonthIndex < 0) {
			month--;
			if (month == 0) {
				month = 12;
				year--;
			}
			dayOfMonthIndex += getTotalDaysForMonth(month, year);
		}
		return new Date(year, month, dayOfMonthIndex + 1);
	}
	
	/**
	 * Create date by using more common argument order (germany): d,m,y
	 */
	public static Date getDate(int dayOfMonth, int monthOfYear, int year) {
		return new Date(year, monthOfYear, dayOfMonth);
	}
	
	public static Date roundToStartOfWeek(Date date) {
		return Date.getDateFromDifference(date, -1 * (date.getDayOfWeek() - 1));
	}
	
	public static Date roundToEndOfWeek(Date date) {
		return Date.getDateFromDifference(date, 7 - date.getDayOfWeek());
	}
	
	/**
	 * Member ----------------------------------------
	 */
	
	private static final long serialVersionUID = -1229452115677333574L;
	
	private final int year;
	private final int monthOfYear;
	private final int dayOfMonth;
	
	private final int dayOfWeek;
	private final int elapsedDaysForYear;
	private final int weekOfYear;
	private int yearOfWeek;
	
	/**
	 * Create new date.
	 * Throws {@link IllegalArgumentException} if illegal date is entered.
	 * All dates that do not exist in ISO normed calendar are illegal.
	 * 
	 * All getter methods return one-based values, not zero-based values!
	 * 
	 * @author Jan Mothes
	 */
	public Date(int year, int monthOfYear, int dayOfMonth) {
		
		this.year = year;
		
		if (monthOfYear < 1 || monthOfYear > 12) throw new IllegalArgumentException("Wrong input for month");
		this.monthOfYear = monthOfYear;
		
		int maxDays = getTotalDaysForMonth(monthOfYear, year);
		if (dayOfMonth < 1 || dayOfMonth > maxDays) throw new IllegalArgumentException("Wrong input for day");
		this.dayOfMonth = dayOfMonth;		
		elapsedDaysForYear = getElapsedDaysForYear();
		dayOfWeek = findDayOfWeek();
		weekOfYear = findWeekOfYear();
	}
	
	/**
	 * @return true if this date points to before the argument date.
	 */
	public boolean isBefore(Date date) {
		if (year < date.year) return true;
		else if (year == date.year) {
			if (monthOfYear < date.monthOfYear) return true;
			else if (monthOfYear == date.monthOfYear) {
				if (dayOfMonth < date.dayOfMonth) return true;
			}
		}
		return false;
	}
	
	/**
	 * @return dayOfYear
	 */
	private int getElapsedDaysForYear() {
		int days = 0;
		for (int i = 1; i < monthOfYear; i++) {
			days += getTotalDaysForMonth(i, year);
		}
		days += dayOfMonth;
		return days;
	}
	
	/*
	 * negative if this date is prior to date argument
	 */
	private int getDifferenceInDays(Date date) {
		final boolean before = isBefore(date);
		Date start = before ? this : date;
		Date end = before ? date : this;
		return getDifferenceInDays(start, end) * (before ? -1 : 1);
	}
	
	protected int findDayOfWeek() {
		final int days = getDifferenceInDays(MONDAY);
		return modulo(days, 7)+1;
	}
	
	/**
	 * '%' is no real modulo operation
	 * @param mod - must be positive
	 */
	private int modulo(int i, int mod) {
		if (i>=0) return i % mod;
		else {
			while (i<0) {
				i = i+mod;
			}
			return i;
		}
	}
	
	/**
	 * Calculates which week of the year this date belongs to.
	 * The year of the week can differ from the actual year of the date!
	 * The year of the week is saved in {@link #yearOfWeek}.
	 * It is only printed if it differs from year of date.
	 */
	private int findWeekOfYear() {
		final int totalDaysOfYear = getTotalDaysForYear(year);
		
		int daysUntilLastThursday = dayOfWeek - 4;
		if (daysUntilLastThursday < 0) daysUntilLastThursday += 7;
		final int lastThursday = elapsedDaysForYear - daysUntilLastThursday;
		
		if (monthOfYear >= 12) {
			//woche zählt schon zum nächsten jahr ?
			final int remainingFromLastThursday = totalDaysOfYear - elapsedDaysForYear + daysUntilLastThursday;
			final int lastDayInThisYearsWeeks = totalDaysOfYear - modulo(remainingFromLastThursday, 7) + 3;
			if (elapsedDaysForYear > lastDayInThisYearsWeeks) {
				yearOfWeek = year+1;
				return 1;
			}
		}
		
		final int firstThursdayOfYear = modulo(elapsedDaysForYear - dayOfWeek + 3, 7) + 1;
		
		if (monthOfYear <= 1) {
			//woche zählt noch zum letzten jahr ?
			int firstDayInThisYearsWeaks = firstThursdayOfYear - 3;
			if (firstDayInThisYearsWeaks > elapsedDaysForYear) {
				yearOfWeek = year-1;
				//53 wenn dieses jahr mit freitag beginnt, sonst 52
				return firstThursdayOfYear==7 ? 53 : 52;
			}
		}
		
		//woche zählt zu diesem Jahr
		yearOfWeek = year;
		final int lastThurdaysWeekOfYear = (int)((7+lastThursday - firstThursdayOfYear) / 7 + 0.5);//rundung theoretisch unnötig
		return (daysUntilLastThursday > 3) ? lastThurdaysWeekOfYear + 1 : lastThurdaysWeekOfYear;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Date) {
			Date date = (Date) obj;
			if (year == date.year && monthOfYear == date.monthOfYear && dayOfMonth == date.dayOfMonth) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int compareTo(Date o) {
		if (equals(o)) return 0;
		return isBefore(o) ? -1 : 1;
	}
	
	@Override
	public String toString() {
		return String.format("%02d", dayOfMonth) + "."
				+ String.format("%02d", monthOfYear) + "."
				+ String.format("%04d", year) + ", "
				+ toDayOfWeekString() + ", "
				+ toWeekOfYear();
	}
	
	public String toWeekOfYear() {
		return "Week: "+weekOfYear + (yearOfWeek!=year ? " ("+yearOfWeek+")" : "");
	}
	
	public String toDayOfWeekString() {
		switch (dayOfWeek) {
		case 1: return "MONDAY";
		case 2: return "TUESDAY";
		case 3: return "WEDNESDAY";
		case 4: return "THURSDAY";
		case 5: return "FRIDAY";
		case 6: return "SATURDAY";
		case 7: return "SUNDAY";
		default:
			throw new IllegalStateException("Day of week is not valid");
		}
	}
	
	public int getYear() {
		return year;
	}
	
	public int getMonthOfYear() {
		return monthOfYear;
	}
	
	public int getDayOfMonth() {
		return dayOfMonth;
	}
	
	/**
	 * @return a day from 1 (Monday) to 7 (Sunday)
	 */
	public int getDayOfWeek() {
		return dayOfWeek;
	}
	
	public int getWeekOfYear() {
		return weekOfYear;
	}
	
	public int getYearOfWeek() {
		return yearOfWeek;
	}
	
	public int getDayOfYear() {
		return Date.getDifferenceInDays(new Date(year, 0, 0), this) + 1;
	}
}
