package testat.shared.model;

import java.io.Serializable;
import java.util.Objects;


/**
 * Represents structure of data of an appointment.
 * 
 * @author Jan Mothes
 */
public class Appointment implements Serializable {
	
	private static final long serialVersionUID = -7423441712926443153L;
	
	private static long idCount = 0;
	
	public static void onLoad(Iterable<? extends Appointment> appos) {
		for(Appointment appo : appos) {
			if(appo.id > idCount) idCount = appo.id;
		}
	}
	
	public static Appointment getFromId(Iterable<? extends Appointment> appos, long id) {
		for(Appointment appo : appos) {
			if(appo.id == id) return appo;
		}
		return null;
	}
	
	private long id;
	
	private String name;
	private String description;
	private DateTime dateStart;
	private DateTime dateEnd;
	private boolean hasEnd = false;
	
	
	public Appointment() {
		this("", "", DateTime.now());
	}
	
	public Appointment(String name) {
		this(name, "", DateTime.now());
	}
	
	public Appointment(String name, String description, DateTime dateStart) {
		this(name, description, dateStart, dateStart, false);
	}
	
	public Appointment(String name, String description,
			DateTime dateStart, DateTime dateEnd, boolean hasEnd) {
		updateId();
		
		setName(name);
		setDescription(description);
		setDate(dateStart, dateEnd);
		setHasEnd(hasEnd);
	}
	
	public void updateId() {
		id = ++idCount;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Appointment other = (Appointment) obj;
		if (id != other.id) return false;
		return true;
	}
	
	/**
	 * Used for "table" view.
	 */
	public String[] toStringArray() {
		return new String[] {
			""+id, name, description, dateStart.toString()
//			, dateEnd.toString(), (hasEnd ? "y" : " ")
		};
	}
	
	@Override
	public String toString() {
		return "Appointment [Name: " + name + ", Desc: " + description
				+ ", Date/Time Start: " + dateStart.toString()
				+ (hasEnd ? ", Date/Time End: " + dateEnd.toString() : "") + "]";
	}
	
	public void setName(String name) {
		Objects.requireNonNull(name);
		this.name = name;
	}
	
	public void setDescription(String description) {
		Objects.requireNonNull(description);
		this.description = description;
	}
	
	public void setDate(DateTime dateStart, DateTime dateEnd) {
		Objects.requireNonNull(dateStart);
		Objects.requireNonNull(dateEnd);
		if (dateEnd.isBefore(dateStart)) throw new IllegalArgumentException(
				"End date cannot be set to before start date!");
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
	}

//	public void setDateEnd(DateTime dateEnd) {
//		Objects.requireNonNull(dateEnd);
//		this.dateEnd = dateEnd;
//	}
	
	public void setHasEnd(boolean hasEnd) {
		this.hasEnd = hasEnd;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public DateTime getDateStart() {
		return dateStart;
	}
	
	public DateTime getDateEnd() {
		return dateEnd;
	}
	
	public DateTime getDateEndEstimate() {
		if (hasEnd()) return dateEnd;
		return new DateTime(Date.getDateFromDifference(dateStart, 1),
				dateEnd.getHour(), dateEnd.getMinute());
	}

	public boolean hasEnd() {
		return hasEnd;
	}
}
