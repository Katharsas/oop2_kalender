package testat.shared.model;

import java.util.Calendar;

/**
 * Represents immutable date & time:
 * 
 * @author Jan
 */
public class DateTime extends Date {

	private static final long serialVersionUID = -7230376932577238470L;
	
	public static double getDifferenceInDaysExact(DateTime from, DateTime to) {
		if (from.equals(to)) return 0;
		if (to.isBefore(from)) return -1 * getDifferenceInDaysExactStrict(to, from);
		return getDifferenceInDaysExactStrict(from, to);
	}
	
	/**
	 * "from" date MUST be prior to "to" date!
	 */
	private static double getDifferenceInDaysExactStrict(DateTime from, DateTime to) {
		double daysExact = Date.getDifferenceInDays(from, to);
		double hourExact = (to.minute - from.minute) / 60;
		daysExact += (hourExact + to.hour - from.hour) / 24;
		return daysExact;
	}
	
	public static DateTime now() {
		Calendar now = Calendar.getInstance();
		DateTime result = new DateTime(now.get(Calendar.YEAR),
				now.get(Calendar.MONTH) + 1, now.get(Calendar.DAY_OF_MONTH),
				now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE));
		return result;
	}
	
	private final int hour;
	private final int minute;
	
	public DateTime(Date date) {
		this(date, 0, 0);
	}
	
	public DateTime(Date date, int hour, int minute) {
		this(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(), hour, minute);
	}
	
	public DateTime(int year, int monthOfYear, int dayOfMonth) {
		this(year, monthOfYear, dayOfMonth, 0, 0);
	}
	
	public DateTime(int year, int monthOfYear, int dayOfMonth, int hour, int minute) {
		super(year, monthOfYear, dayOfMonth);
		if(hour < 0 || hour > 23) throw new IllegalArgumentException("Wrong input for hour");
		if(minute < 0 || minute > 59) throw new IllegalArgumentException("Wrong input for minute");
		this.hour = hour;
		this.minute = minute;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hour;
		result = prime * result + minute;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!super.equals(obj)) return false;
		if (getClass() != obj.getClass()) return false;
		DateTime other = (DateTime) obj;
		return hour == other.hour && minute == other.minute;
	}

	private boolean isBefore(DateTime date) {
		if(super.isBefore(date)) return true;
		else if (hour < date.hour) {
			if (minute < date.minute) return true;
		}
		return false;
	}
	
	public int compareTo(DateTime o) {
		if (o == null) throw new IllegalArgumentException();
		if (equals(o)) return 0;
		return isBefore(o) ? -1 : 1;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", "
				+ (hour < 10 ? " " : "") + hour
				+ ":" + String.format("%02d", minute);
	}
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
}
