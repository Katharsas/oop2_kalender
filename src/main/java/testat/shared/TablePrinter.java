package testat.shared;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Prints a table of strings.
 * Use the method {@link #addRow(String[])} or the methods
 * {@link #add(String)} & {@link #finishRow()} to fill the table (or both).
 * 
 * You can add as many cells per row as you like (for every row).
 * Missing cells will be added as empty cells.
 * Cells are always big enough to fit all text inside.
 * 
 * @author Jan Mothes
 */
public class TablePrinter {
	private final String nl = "\n";
	private final List<List<String>> columns = new LinkedList<List<String>>();
	private final List<String> row = new LinkedList<String>();
	private int[] minWidth;
	
	public TablePrinter setMinWidth(int[] minWidth) {
		this.minWidth = minWidth;
		return this;
	}
	public TablePrinter add(String cell) {
		if (cell==null) throw new NullPointerException();
		row.add(cell);
		return this;
	}
	public TablePrinter finishRow() {
		columns.add(new ArrayList<String>(row));
		row.clear();
		return this;
	}
	public TablePrinter addRow(String[] row) {
		if (row==null) throw new NullPointerException();
		for (String cell : row) {
			add(cell);
		}
		finishRow();
		return this;
	}
	@Override
	public String toString() {
		//calculate widths
		final ArrayList<Integer> cellWidths = new ArrayList<Integer>();
		for (List<String> row : columns) {
			for (int i = 0; i<row.size(); i++) {
				if (cellWidths.size()-1 < i) cellWidths.add(0);
				int width = cellWidths.get(i);
				int newWidth = row.get(i).length();
				if (minWidth != null) {
					if (minWidth.length > i && newWidth < minWidth[i]) newWidth = minWidth[i];
				}
				if (newWidth > width) cellWidths.set(i, newWidth);
			}
		}
		//line
		String line = "-";
		for (int i : cellWidths) {
			line += getString(i+3, '-');
		}
		line += nl;
		//print table
		String result = line;
		for (List<String> row : columns) {
			result += "|";
			for (int i = 0; i < cellWidths.size(); i++) {
				result += " ";
				String cell = "";
				if (row.size() > i) cell = row.get(i);
				int space = cellWidths.get(i) - cell.length();
				result += cell + getString(space, ' ');
				result += " |";
			}
			result += nl + line;
		}
		return result;
	}
	private String getString(int length, char c) {
		String s = "";
		for (int j = 0; j<length; j++) {
			s += c;
		}
		return s;
	}
}